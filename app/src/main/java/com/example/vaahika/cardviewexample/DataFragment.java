package com.example.vaahika.cardviewexample;


import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONObject;


/**
 * A simple {@link Fragment} subclass.
 */
public class DataFragment extends Fragment {

    String data;

    public DataFragment() {}


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_data, container, false);
        try {
            if(getArguments() != null) {
            data = getArguments().getString("message");
            ((TextView) v.findViewById(R.id.amount_data)).setText(new JSONObject(new JSONArray(data).get(getArguments().getInt("no")).toString()).getString("amount"));
            ((TextView) v.findViewById(R.id.remark_data)).setText(new JSONObject(new JSONArray(data).get(getArguments().getInt("no")).toString()).getString("remark"));
            ((TextView) v.findViewById(R.id.rating_data)).setText(new JSONObject(new JSONArray(data).get(getArguments().getInt("no")).toString()).getString("rating"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return v;
    }

}
