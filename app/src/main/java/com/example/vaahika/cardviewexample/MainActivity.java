package com.example.vaahika.cardviewexample;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;


public class MainActivity extends AppCompatActivity {

    private RecyclerView.Adapter mAdapter;
    private static String LOG_TAG = "MainActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        RecyclerView mRecyclerView = (RecyclerView) findViewById(R.id.my_recycler_view);
        mRecyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mAdapter = new MyRecyclerViewAdapter(getDataSet(),MainActivity.this);
        mRecyclerView.setAdapter(mAdapter);

    }


    private ArrayList<DataObject> getDataSet() {
        ArrayList results = new ArrayList<>();

        try {
            JSONObject object = new JSONObject(Constant.data);
            JSONArray array = new JSONArray(object.getString("today"));
            for (int i = 0; i < array.length(); i++) {
                Log.d(LOG_TAG,array.getString(i));
                DataObject obj = new DataObject(array.getString(i));
                results.add(i, obj);
            }
        } catch(JSONException e) {
            e.printStackTrace();
        }

        return results;
    }
}