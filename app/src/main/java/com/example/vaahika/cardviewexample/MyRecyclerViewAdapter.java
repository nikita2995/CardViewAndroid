package com.example.vaahika.cardviewexample;

import android.app.Activity;
import android.app.FragmentTransaction;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;

public class MyRecyclerViewAdapter extends RecyclerView
        .Adapter<MyRecyclerViewAdapter
        .DataObjectHolder> {
    private static String LOG_TAG = "MyRecyclerViewAdapter";
    private ArrayList<DataObject> mDataset;
    private static Context context;
    LayoutInflater inflater;

    public static class DataObjectHolder extends RecyclerView.ViewHolder {
        TextView title,pick_area,delivery_area,truck_type,item_name,weight,footer;
        FrameLayout container;
        ImageView arrow_left,arrow_right;
        LinearLayout arrow_left_container,arrow_right_container;
        FragmentTransaction fragmentTransaction;
        DataFragment df;

        public DataObjectHolder(View itemView) {
            super(itemView);
            title = (TextView) itemView.findViewById(R.id.title);
            pick_area = (TextView) itemView.findViewById(R.id.pick_area_data);
            delivery_area = (TextView) itemView.findViewById(R.id.delivery_area_data);
            item_name = (TextView) itemView.findViewById(R.id.item_name_data);
            truck_type = (TextView) itemView.findViewById(R.id.truck_type_data);
            weight = (TextView) itemView.findViewById(R.id.weight_data);
            container = (FrameLayout) itemView.findViewById(R.id.container);
            footer = (TextView)itemView.findViewById(R.id.footer);
            arrow_left = (ImageView) itemView.findViewById(R.id.arrow_left);
            arrow_right = (ImageView) itemView.findViewById(R.id.arrow_right);
            arrow_left_container = (LinearLayout) itemView.findViewById(R.id.arrow_left_container);
            arrow_right_container = (LinearLayout) itemView.findViewById(R.id.arrow_right_container);
            fragmentTransaction = ((Activity)context).getFragmentManager().beginTransaction();
            df = new DataFragment();
            arrow_left.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Bundle data = new Bundle();
                    fragmentTransaction = ((Activity)context).getFragmentManager().beginTransaction();
                    try {
                        if (new JSONArray(df.getArguments().getString("message")).length() <= (df.getArguments().getInt("no") + 1)) {
                            data.putInt("no", 0);
                        } else {
                            data.putInt("no", df.getArguments().getInt("no") + 1);
                        }
                    } catch(JSONException e){
                        e.printStackTrace();
                    }
                    data.putString("message",df.getArguments().getString("message"));
                    df =new DataFragment();
                    df.setArguments(data);
                    fragmentTransaction.setCustomAnimations(R.animator.fragment_slide_left_enter,
                            R.animator.fragment_slide_left_exit,R.animator.fragment_slide_right_enter,R.animator.fragment_slide_right_exit);
                    fragmentTransaction.replace(container.getId(),df);
                    fragmentTransaction.commit();
                }
            });
            arrow_right.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Bundle data = new Bundle();
                    fragmentTransaction = ((Activity)context).getFragmentManager().beginTransaction();
                    try {
                        if ((df.getArguments().getInt("no") - 1)<0) {
                            data.putInt("no", new JSONArray(df.getArguments().getString("message")).length()-1);
                        } else {
                            data.putInt("no", df.getArguments().getInt("no") - 1);
                        }
                    } catch(JSONException e){
                        e.printStackTrace();
                    }
                    data.putString("message",df.getArguments().getString("message"));
                    df =new DataFragment();
                    df.setArguments(data);
                    fragmentTransaction.setCustomAnimations(R.animator.fragment_slide_right_enter,R.animator.fragment_slide_right_exit,R.animator.fragment_slide_left_enter,R.animator.fragment_slide_left_exit);
                    fragmentTransaction.replace(container.getId(),df);

                    fragmentTransaction.commit();
                }
            });
            container.setOnTouchListener(new View.OnTouchListener() {
                private int min_distance = 100;
                private float downX, downY, upX, upY;
                View v;

                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    this.v = v;
                    switch(event.getAction()) { // Check vertical and horizontal touches
                        case MotionEvent.ACTION_DOWN: {
                            downX = event.getX();
                            downY = event.getY();
                            return true;
                        }
                        case MotionEvent.ACTION_UP: {
                            upX = event.getX();
                            upY = event.getY();

                            float deltaX = downX - upX;
                            float deltaY = downY - upY;

                            //HORIZONTAL SCROLL
                            if (Math.abs(deltaX) > Math.abs(deltaY)) {
                                if (Math.abs(deltaX) > min_distance) {
                                    // left or right
                                    if (deltaX < 0) {
                                        this.onLeftToRightSwipe();
                                        return true;
                                    }
                                    if (deltaX > 0) {
                                        this.onRightToLeftSwipe();
                                        return true;
                                    }
                                } else {
                                    //not long enough swipe...
                                    return false;
                                }
                            }
                            return false;
                        }
                    }
                    return false;
                }

                public void onLeftToRightSwipe(){
                    Bundle data = new Bundle();
                    fragmentTransaction = ((Activity)context).getFragmentManager().beginTransaction();
                    try {
                        if ((df.getArguments().getInt("no") - 1)<0) {
                            data.putInt("no", new JSONArray(df.getArguments().getString("message")).length()-1);
                        } else {
                            data.putInt("no", df.getArguments().getInt("no") - 1);
                        }
                    } catch(JSONException e){
                        e.printStackTrace();
                    }
                    data.putString("message",df.getArguments().getString("message"));
                    df =new DataFragment();
                    df.setArguments(data);
                    fragmentTransaction.setCustomAnimations(R.animator.fragment_slide_right_enter,R.animator.fragment_slide_right_exit,R.animator.fragment_slide_left_enter,R.animator.fragment_slide_left_exit);
                    fragmentTransaction.replace(container.getId(),df);

                    fragmentTransaction.commit();
                }

                public void onRightToLeftSwipe() {
                    Bundle data = new Bundle();
                    fragmentTransaction = ((Activity)context).getFragmentManager().beginTransaction();
                    try {
                        if (new JSONArray(df.getArguments().getString("message")).length() <= (df.getArguments().getInt("no") + 1)) {
                            data.putInt("no", 0);
                        } else {
                            data.putInt("no", df.getArguments().getInt("no") + 1);
                        }
                    } catch(JSONException e){
                        e.printStackTrace();
                    }
                    data.putString("message",df.getArguments().getString("message"));
                    df =new DataFragment();
                    df.setArguments(data);
                    fragmentTransaction.setCustomAnimations(R.animator.fragment_slide_left_enter,
                            R.animator.fragment_slide_left_exit,R.animator.fragment_slide_right_enter,R.animator.fragment_slide_right_exit);
                    fragmentTransaction.replace(container.getId(),df);
                    fragmentTransaction.commit();
                }
            });
        }
    }

    public MyRecyclerViewAdapter(ArrayList<DataObject> myDataset,Context context) {
        this.context = context;
        mDataset = myDataset;
        inflater = LayoutInflater.from(context);
        setHasStableIds(false);
    }

    @Override
    public DataObjectHolder onCreateViewHolder(ViewGroup parent,
                                               int viewType) {
        View view = inflater.inflate(R.layout.card_view_rom, parent, false);

        return new DataObjectHolder(view);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public void onBindViewHolder(DataObjectHolder holder, int position) {
        holder.title.setText(mDataset.get(position).getTitle());
        holder.pick_area.setText(mDataset.get(position).getPick_area());
        holder.delivery_area.setText(mDataset.get(position).getDelivery_area());
        holder.item_name.setText(mDataset.get(position).getItem_name());
        holder.truck_type.setText(mDataset.get(position).getTruck_type());
        holder.weight.setText(mDataset.get(position).getWeight());
        Bundle data = new Bundle();
        data.putString("message",mDataset.get(position).getData());
        data.putInt("no",0);
        holder.df.setArguments(data);
        holder.container.setId(getUniqueId(position));
        holder.fragmentTransaction.replace(holder.container.getId(),holder.df,""+position);

        holder.fragmentTransaction.commit();
        //holder.container.setId(old_id);
        //Log.d(LOG_TAG,holder.container.getId()+"");
        holder.arrow_right_container.setGravity(Gravity.CENTER);
        holder.arrow_left_container.setGravity(Gravity.CENTER);
    }

    private int getUniqueId(int position) {
        return position+1;
    }

    public void addItem(DataObject dataObj, int index) {
        mDataset.add(index, dataObj);
        notifyItemInserted(index);
    }

    public void deleteItem(int index) {
        mDataset.remove(index);
        notifyItemRemoved(index);
    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }
}