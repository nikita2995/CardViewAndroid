package com.example.vaahika.cardviewexample;

/**
 * Created by vaahika on 19/7/16.
 */
public class Constant {
    static String data = "{" +
            "\"plus2\": []," +
            "\"plus1\": []," +
            "\"today\": [{" +
            "\"title\": \"Jaipur To Delhi\"," +
            "\"pickArea\": \"Sitapura\"," +
            "\"deliveryArea\": \"Industrial Area, Mangolpuri\"," +
            "\"itemName\": \"Steel Pipe\"," +
            "\"truckType\": \"32 Ton Body (4921 Series)\"," +
            "\"weight\": \"25 Tones\"," +
            "\"bids\": [{" +
            "\"amount\": \"5000\"," +
            "\"remark\": \"2 Ft Height, Dala Open.\"," +
            "\"rating\": \"5\"" +
            "}, {" +
            "\"amount\": \"5550\"," +
            "\"remark\": \"2 Ft Height, Dala Open.\"," +
            "\"rating\": \"5\"" +
            "}]" +
            "}, {" +
            "\"title\": \"Ajmer To Delhi\"," +
            "\"pickArea\": \"Sitapura\"," +
            "\"deliveryArea\": \"Industrial Area, Mangolpuri\"," +
            "\"itemName\": \"Steel Pipe\"," +
            "\"truckType\": \"3211111 Ton Body (4921 Series)\"," +
            "\"weight\": \"25 Tones\"," +
            "\"bids\": [{" +
            "\"amount\": \"55500\"," +
            "\"remark\": \"2 Ft Height, Dala Open.\"," +
            "\"rating\": \"5\"" +
            "}, {" +
            "\"amount\": \"6000\"," +
            "\"remark\": \"2 Ft Height, Dala Open.\"," +
            "\"rating\": \"5\"" +
            "}, {" +
            "\"amount\": \"9000\"," +
            "\"remark\": \"2 Ft Height, Dala Open.\"," +
            "\"rating\": \"5\"" +
            "}]" +
            "}, {" +
            "\"title\": \"Jaipur To Delhi\"," +
            "\"pickArea\": \"Sitapura\"," +
            "\"deliveryArea\": \"Industrial Area, Mangolpuri\"," +
            "\"itemName\": \"Steel Pipe\"," +
            "\"truckType\": \"32 Ton Body (4921 Series)\"," +
            "\"weight\": \"25 Tones\"," +
            "\"bids\": [{" +
            "\"amount\": \"5000\"," +
            "\"remark\": \"2 Ft Height, Dala Open.\"," +
            "\"rating\": \"5\"" +
            "}, {" +
            "\"amount\": \"5550\"," +
            "\"remark\": \"2 Ft Height, Dala Open.\"," +
            "\"rating\": \"5\"" +
            "}]" +
            "}, {" +
            "\"title\": \"Ajmer To Delhi\"," +
            "\"pickArea\": \"Sitapura\"," +
            "\"deliveryArea\": \"Industrial Area, Mangolpuri\"," +
            "\"itemName\": \"Steel Pipe\"," +
            "\"truckType\": \"3211111 Ton Body (4921 Series)\"," +
            "\"weight\": \"25 Tones\"," +
            "\"bids\": [{" +
            "\"amount\": \"55500\"," +
            "\"remark\": \"2 Ft Height, Dala Open.\"," +
            "\"rating\": \"5\"" +
            "}, {" +
            "\"amount\": \"6000\"," +
            "\"remark\": \"2 Ft Height, Dala Open.\"," +
            "\"rating\": \"5\"" +
            "}, {" +
            "\"amount\": \"9000\"," +
            "\"remark\": \"2 Ft Height, Dala Open.\"," +
            "\"rating\": \"5\"" +
            "}]" +
            "}]," +
            "\"minus1\": []," +
            "\"minus2\": []" +
            "}";
}
