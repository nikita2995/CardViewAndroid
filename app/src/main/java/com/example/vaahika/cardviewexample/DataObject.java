package com.example.vaahika.cardviewexample;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by vaahika on 18/7/16.
 */
public class DataObject {
    private String title;
    private String pick_area;
    private String delivery_area;
    private String item_name;
    private String truck_type;
    private String weight;
    private String data;

    DataObject (String text1){
        try {
            this.title = new JSONObject(text1).getString("title");
            this.pick_area = new JSONObject(text1).getString("pickArea");
            this.delivery_area = new JSONObject(text1).getString("deliveryArea");
            this.item_name = new JSONObject(text1).getString("itemName");
            this.truck_type = new JSONObject(text1).getString("truckType");
            this.weight = new JSONObject(text1).getString("weight");
            this.data = new JSONObject(text1).getString("bids");

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPick_area() {
        return pick_area;
    }

    public void setPick_area(String pick_area) {
        this.pick_area = pick_area;
    }

    public String getDelivery_area() {
        return delivery_area;
    }

    public void setDelivery_area(String delivery_area) {
        this.delivery_area = delivery_area;
    }

    public String getItem_name() {
        return item_name;
    }

    public void setItem_name(String item_name) {
        this.item_name = item_name;
    }

    public String getTruck_type() {
        return truck_type;
    }

    public void setTruck_type(String truck_type) {
        this.truck_type = truck_type;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }
}
